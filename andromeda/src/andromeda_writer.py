import json
from pyspark.sql import SparkSession
from pyspark.sql.functions import col, lit
from datetime import datetime, timedelta
from libs.andromeda_helper import *
from libs.alert import Alert
from libs.EasyMySQL import EasyMySQL

if __name__ == '__main__':
    spark = SparkSession.builder.enableHiveSupport()\
        .config('spark.jars.packages', 'mysql:mysql-connector-java:5.1.39')\
        .config("spark.sql.autoBroadcastJoinThreshold", -1)\
        .getOrCreate()

    sc = spark.sparkContext
    logger = get_logger('info')
    logger.info("SparkContext configs: {spark_conf_all}".format(
        spark_conf_all=sc.getConf().getAll()))

    ### read configs ###
    logger.info("Reading configs...")
    with open('config.json') as config_file:
        config = json.load(config_file)

    username_db_aurora = config['username_db_aurora']
    password_db_aurora = config['password_db_aurora']
    sample_fraction = config['sample_fraction']
    max_time_span = config['max_time_span']
    custom_period = config['custom_period']
    custom_from_date = config['custom_from_date']
    custom_to_date = config['custom_to_date']
    taxonomy_s3_location = config['taxonomy_s3_location']
    alert_enabled = config['alert_enabled']
    teams_webhooks = config['teams_webhooks']
    job_name = config['job_name']

    ### initialize alert ###
    logger.info("Initializing alert...")
    alert = Alert(logger, teams_webhooks, alert_enabled)

    alert.send_alert("Andromeda: START!")

    min_date = datetime.datetime.now() - datetime.timedelta(days=1) - \
        datetime.timedelta(days=max_time_span)
    max_date = datetime.datetime.now() - datetime.timedelta(days=1)

    from_date = "{year}-{month}-{day}".format(year=min_date.year, month=str(min_date.month).zfill(2),
                                              day=str(min_date.day).zfill(2))
    to_date = "{year}-{month}-{day}".format(year=max_date.year, month=str(max_date.month).zfill(2),
                                            day=str(max_date.day).zfill(2))

    if custom_period:
        from_date = custom_from_date
        to_date = custom_to_date

    logger.info("SparkContext configs: {spark_conf_all}".format(
        spark_conf_all=sc.getConf().getAll()))

    logger.info(f"""Starting TVA-DASHBOARD data computation from {from_date} to {to_date} [{max_time_span} days]""")

    logger.info("""Trying to connect to Aurora MySQL DB...""")
    aurora_driver = EasyMySQL(
        "dashboard_profile", username_db_aurora, password_db_aurora)
    logger.info("""Connection established!""")

    # ---|| CORE VIEWER ||---#
    logger.info("""Computing enabler fruitions...""")
    dl_enabler_action_view_palinsesto = get_logged_enabler_fruitions(
        spark, from_date, to_date)
    logger.info("""Computing individual cats...""")
    dl_pr_individual_cats = get_cats(spark, min_date, max_date)
    logger.info("""Computing ranked video event...""")
    dl_da_ranked_video_event = get_ranked(spark, from_date, to_date)

    logger.info("""Computing CORE_VIEWER...""")
    dl_enabler_action_view_palinsesto.createOrReplaceTempView(
        "dl_enabler_action_view_palinsesto")
    dl_pr_individual_cats.createOrReplaceTempView("dl_pr_individual_cats")
    dl_da_ranked_video_event.createOrReplaceTempView(
        "dl_da_ranked_video_event")

    core_viewers = spark.sql("""SELECT uuid,
									profile_gender AS gender,
									profile_age AS age,
									data_province AS province,
									registrationsource AS registration_source,
									loginprovider AS login_provider,
									DATE(SUBSTRING(created,1,10)) AS registration_date
								FROM dl_mediaset.dl_gg_profile
								WHERE uuid IN (SELECT user_uid FROM dl_da_ranked_video_event) OR
								uuid in (SELECT userid FROM dl_enabler_action_view_palinsesto) OR
								uuid in (SELECT ind_id FROM dl_pr_individual_cats)""").dropDuplicates().sample(False,
                                                                                       sample_fraction,
                                                                                       74)
    core_viewers.cache()
    core_viewers.createOrReplaceTempView("core_viewer")

    core_viewers_partitions = core_viewers.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEWER: {core_viewers_partitions} partitions""")

    core_viewers_table_name = 'core_viewer_100'
    core_viewers_schema = """CREATE TABLE `{table_name}` (
	`uuid` varchar(200) NOT NULL,
	`gender` varchar(50) DEFAULT NULL,
	`age` int(11) DEFAULT NULL,
	`province` varchar(200) DEFAULT NULL,
	`registration_source` varchar(100) DEFAULT NULL,
	`login_provider` varchar(100) DEFAULT NULL,
	`registration_date` date DEFAULT NULL,
	PRIMARY KEY (`uuid`),
    KEY `{table_name}_gender_4f0a68d3` (`gender`),
    KEY `{table_name}_age_d92b279e` (`age`),
    KEY `{table_name}_login_provider_e7de3214` (`login_provider`),
    KEY `{table_name}_province_7d89f131` (`province`),
    KEY `{table_name}_registration_source_74fa8c3b` (`registration_source`),
    KEY `{table_name}_registration_date_d12d0068` (`registration_date`),
    KEY `{table_name}_age_IDX` (`age`,`registration_date`),
    KEY `{table_name}_idx_uuid_registration_da` (`uuid`,`registration_date`),
    KEY `{table_name}_idx_uuid_age` (`uuid`,`age`),
    KEY `{table_name}_idx_gender_uuid_age` (`gender`,`uuid`,`age`),
    KEY `{table_name}_idx_gender_age` (`gender`,`age`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8""".format(table_name=core_viewers_table_name)

    #write_df_to_aurora(logger, core_viewers, core_viewers_table_name, aurora_driver, core_viewers_schema)

    # ---|| CORE DMP EVENT ||---#
    taxonomy_machine = get_taxonomy(spark, taxonomy_s3_location)
    dmp_event = dl_pr_individual_cats \
        .join(taxonomy_machine, dl_pr_individual_cats.cat == taxonomy_machine.id, 'left') \
        .withColumn("id", F.lit(0)) \
        .withColumnRenamed("ind_id", "viewer_id") \
        .withColumnRenamed("name", "interest") \
        .withColumnRenamed("cat_sum_freq", "freq") \
        .filter(F.col("interest").isNotNull()) \
        .select("id", "date", "interest", "path", "freq", "viewer_id")

    logger.info("""Computing CORE_DMP_EVENT...""")
    dmp_event.createOrReplaceTempView("dmp_event")

    core_dmp_event = spark.sql("""SELECT *
								FROM dmp_event
								WHERE viewer_id IN (SELECT uuid FROM core_viewer)""")
    core_dmp_event.cache()

    core_dmp_event_partitions = core_dmp_event.rdd.getNumPartitions()
    logger.info(f"""CORE_DMP_EVENT: {core_dmp_event_partitions} partitions""")

    core_dmp_event_table_name = 'core_dmpevent_100'
    core_dmp_event_schema = """CREATE TABLE `{table_name}` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`interest` varchar(100) DEFAULT NULL,
	`path` varchar(200) DEFAULT NULL,
	`freq` decimal(10,3) NOT NULL,
	`viewer_id` varchar(200) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `{table_name}_viewer_id_9938f663_fk_{reference}_uuid` (`viewer_id`),
	KEY `{table_name}_date_cf38a4f2` (`date`),
	KEY `{table_name}_interest_eca4c0e6` (`interest`),
	CONSTRAINT `{table_name}_viewer_id_9938f663_fk_{reference}_uuid` FOREIGN KEY (`viewer_id`) REFERENCES `{reference}` (`uuid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8""".format(table_name=core_dmp_event_table_name, reference=core_viewers_table_name)

    #write_df_to_aurora(logger, core_dmp_event, core_dmp_event_table_name, aurora_driver, core_dmp_event_schema)

    # ---|| CORE VIEW EVENT: K||---#
    dl_enabler_mcm_anagrafica = spark.sql("""SELECT codice_f, 
													titolo, 
													tipologia, 
													titolo_ui_brand,
													canale_ultima_emissione
											FROM dl_mediaset.dl_enabler_mcm_anagrafica""")

    view_event_kin = dl_da_ranked_video_event \
        .join(dl_enabler_mcm_anagrafica, dl_da_ranked_video_event.video_id == dl_enabler_mcm_anagrafica.codice_f, 'left') \
        .withColumn("real_tts", F.when((F.col("tts") > F.lit(21600)) & (F.col("video_play_request_type") == 'live'),
                                       F.lit(0)).otherwise(F.col("tts"))) \
        .withColumn("agg_titolo", F.when((F.col("titolo_ui_brand") != '') & (F.col("titolo_ui_brand").isNotNull()),
                                         F.col("titolo_ui_brand")).otherwise(F.col("titolo"))) \
        .groupBy("video_play_request_type", "user_uid", "business_date", "weekday", "agg_titolo") \
        .agg(F.sum("real_tts").alias("watch_time"),
             F.first("canale_ultima_emissione").alias("channel"),
             F.first("tipologia").alias("watch_category"),
             F.first("os_name").alias("os_name")) \
        .filter(F.col("watch_time") > 0) \
        .withColumn("watch_time", F.col("watch_time") * F.lit(1000000)) \
        .withColumn("id", F.lit(0)) \
        .withColumn("date", F.to_date(F.col("business_date"), "yyyy-MM-dd")) \
        .withColumn("daysBetween", F.datediff(F.to_date(F.lit(to_date), "yyyy-MM-dd"), F.col("date"))) \
        .withColumn("days", F.when(F.col("daysBetween") <= F.lit(7), F.lit(7))
                    .when((F.col("daysBetween") > F.lit(7)) & (F.col("daysBetween") <= F.lit(14)), F.lit(14))
                    .when((F.col("daysBetween") > F.lit(14)) & (F.col("daysBetween") <= F.lit(30)), F.lit(30))
                    .otherwise(F.lit(90))) \
        .withColumn("view_type", F.lit('k')) \
        .withColumn("device_type", F.lit(None).cast(StringType())) \
        .withColumn("fruition_type", F.lit(None).cast(StringType())) \
        .withColumnRenamed("video_play_request_type", "play_type") \
        .withColumnRenamed("user_uid", "viewer_id") \
        .withColumnRenamed("agg_titolo", "title") \
        .select("id", "date", "weekday", "view_type", "channel", "title", "device_type", "watch_category", "watch_time",
                "viewer_id",
                "os_name", "fruition_type", "play_type", "days")

    logger.info("""Computing CORE_VIEW_EVENT_KIN...""")
    view_event_kin.createOrReplaceTempView("view_event_kin")

    core_view_event_kin = spark.sql("""SELECT *
								FROM view_event_kin
								WHERE viewer_id IN (SELECT uuid FROM core_viewer)""")
    core_view_event_kin_partitions = core_view_event_kin.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEW_EVENT_KIN: {core_view_event_kin_partitions} partitions""")

    # ---|| CORE VIEW EVENT: E||---#
    dl_enabler_device_id = get_device_id(spark, from_date, to_date)

    en_join_condition = [
        dl_enabler_action_view_palinsesto.deviceid == dl_enabler_device_id.deviceid,
        dl_enabler_action_view_palinsesto.userid == dl_enabler_device_id.userid
    ]

    view_event_en = dl_enabler_action_view_palinsesto \
        .join(dl_enabler_device_id, en_join_condition, 'left') \
        .drop(dl_enabler_device_id.deviceid) \
        .drop(dl_enabler_device_id.userid) \
        .join(dl_enabler_mcm_anagrafica,
              dl_enabler_action_view_palinsesto.codiceprodotto == dl_enabler_mcm_anagrafica.codice_f, 'left') \
        .withColumn("agg_titolo", F.when((F.col("titolo_ui_brand") != '') & (F.col("titolo_ui_brand").isNotNull()),
                                         F.col("titolo_ui_brand")).otherwise(F.col("titolo"))) \
        .groupBy("fruition_type", "userid", "agg_titolo", "tr_date", "weekday") \
        .agg(F.sum("sec_dur").alias("watch_time"),
             F.first("tipodevice").alias("tipodevice"),
             F.first("tipologia").alias("watch_category"),
             F.first("rete").alias("channel")) \
        .filter(F.col("watch_time") > 0) \
        .withColumn("watch_time", F.col("watch_time") * F.lit(1000000)) \
        .withColumn("id", F.lit(0)) \
        .withColumn("date", F.to_date(F.col("tr_date"), "yyyy-MM-dd")) \
        .withColumn("daysBetween", F.datediff(F.to_date(F.lit(to_date), "yyyy-MM-dd"), F.col("date"))) \
        .withColumn("days", F.when(F.col("daysBetween") <= F.lit(7), F.lit(7))
                    .when((F.col("daysBetween") > F.lit(7)) & (F.col("daysBetween") <= F.lit(14)), F.lit(14))
                    .when((F.col("daysBetween") > F.lit(14)) & (F.col("daysBetween") <= F.lit(30)), F.lit(30))
                    .otherwise(F.lit(90))) \
        .withColumn("view_type", F.lit('e')) \
        .withColumn("device_type", F.when(F.col("tipodevice") == F.lit(1), F.lit('MHP - Generic'))
                    .when(F.col("tipodevice") == F.lit(2), F.lit('HbbTV'))
                    .when(F.col("tipodevice") == F.lit(3), F.lit('MHP - Gold'))
                    .when(F.col("tipodevice") == F.lit(4), F.lit('MHP - Tivuon'))) \
        .withColumn("os_name", F.lit(None).cast(StringType())) \
        .withColumn("play_type", F.lit(None).cast(StringType())) \
        .withColumnRenamed("userid", "viewer_id") \
        .withColumnRenamed("agg_titolo", "title") \
        .select("id", "date", "weekday", "view_type", "channel", "title", "device_type", "watch_category", "watch_time",
                "viewer_id",
                "os_name", "fruition_type", "play_type", "days")

    logger.info("""Computing CORE_VIEW_EVENT_EN...""")
    view_event_en.createOrReplaceTempView("view_event_en")

    core_view_event_en = spark.sql("""SELECT *
									FROM view_event_en
									WHERE viewer_id IN (SELECT uuid FROM core_viewer)""")
    core_view_event_en_partitions = core_view_event_en.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEW_EVENT_EN: {core_view_event_en_partitions} partitions""")

    # ---|| CORE VIEW EVENT||---#
    logger.info("""Computing CORE_VIEW_EVENT...""")
    core_view_event = core_view_event_kin.union(core_view_event_en)
    core_view_event.cache()

    core_view_event_partitions = core_view_event.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEW_EVENT: {core_view_event_partitions} partitions""")

    core_view_event_table_name = 'core_viewevent_100'
    core_view_event_schema = """CREATE TABLE `{table_name}` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `{table_name}_viewer_id_6f4e4f04_fk_{reference}_uuid` (`viewer_id`),
	KEY `{table_name}_view_type_0a7186db` (`view_type`),
	KEY `{table_name}_channel_7086078f` (`channel`),
	KEY `{table_name}_date_d4960aad` (`date`),
	KEY `{table_name}_weekday_d4747ffe` (`weekday`),
	KEY `{table_name}_device_type_cc33a1a5` (`device_type`),
	KEY `{table_name}_os_name_3911a3d8` (`os_name`),
	KEY `{table_name}_title_c2530175` (`title`),
	KEY `{table_name}_watch_category_e7033ef9` (`watch_category`),
	KEY `{table_name}_watch_time_34c0d0ef` (`watch_time`),
	KEY `{table_name}_fruition_type_201f5543` (`fruition_type`),
	KEY `{table_name}_play_type_1346a010` (`play_type`),
	KEY `{table_name}_days_afd1aff6` (`days`),
	CONSTRAINT `{table_name}_ibfk_1` FOREIGN KEY (`viewer_id`) REFERENCES `{reference}` (`uuid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8""".format(table_name=core_view_event_table_name, reference=core_viewers_table_name)

    #write_df_to_aurora(logger, core_view_event, core_view_event_table_name, aurora_driver, core_view_event_schema)

    # ---|| CORE VIEW EVENT FLAT||---#
    logger.info("""Computing CORE_VIEW_EVENT_FLAT...""")

    core_view_event_flat = core_view_event \
        .join(core_viewers, core_view_event.viewer_id == core_viewers.uuid, 'inner') \
        .drop(core_view_event.viewer_id) \
        .groupBy("view_type", "title", "device_type", "os_name", "uuid", "days", "fruition_type", "play_type") \
        .agg(F.count("id").alias("count_events"),
             F.sum("watch_time").alias("watch_time"),
             F.first("watch_category").alias("watch_category"),
             F.first("channel").alias("channel"),
             F.first("gender").alias("gender"),
             F.first("province").alias("province"),
             F.first("age").alias("age")) \
        .withColumn("id", F.lit(0)) \
        .withColumnRenamed("uuid", "viewer_id") \
        .select("id", "view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_view_event_flat_partitions = core_view_event_flat.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEW_EVENT_FLAT: {core_view_event_flat_partitions} partitions""")

    core_view_event_flat_table_name = 'core_vieweventflat_100'
    core_view_event_flat_schema = """CREATE TABLE `{table_name}` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`gender` varchar(50) DEFAULT NULL,
	`age` int(11) DEFAULT NULL,
	`province` varchar(200) DEFAULT NULL,
	`count_events` int(11) NOT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`days` int(11) NOT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	PRIMARY KEY (`id`),
	KEY `{table_name}_viewer_id_4f99cd07_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_view_type_9ad59e39` (`view_type`),
    KEY `{table_name}_channel_d37fb77e` (`channel`),
    KEY `{table_name}_title_3a1cc0a2` (`title`),
    KEY `{table_name}_device_type_61c91d4f` (`device_type`),
    KEY `{table_name}_watch_category_5e4c17ea` (`watch_category`),
    KEY `{table_name}_os_name_94cf2917` (`os_name`),
    KEY `{table_name}_gender_aedbbe78` (`gender`),
    KEY `{table_name}_age_fb94ccb8` (`age`),
    KEY `{table_name}_province_81d7eca9` (`province`),
    KEY `{table_name}_days_8806617c` (`days`),
    KEY `{table_name}_fruition_type_72a808d8` (`fruition_type`),
    KEY `{table_name}_play_type_0d14ff43` (`play_type`),
    KEY `{table_name}_days_channel_IDX` (`days`,`channel`),
    KEY `{table_name}_days_channel_viewer_id_IDX` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_watch_category_viewer_id_IDX` (`watch_category`,`viewer_id`),
    KEY `{table_name}_title_viewer_id_IDX` (`title`,`viewer_id`),
    KEY `{table_name}_watch_category_title_viewer_id_IDX` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_gender_days_age_IDX` (`gender`,`days`,`age`),
    KEY `{table_name}_gender_age_watchtime_pt_ft_IDX` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_gender_age_watchtime_IDX` (`gender`,`age`,`watch_time`),
    KEY `{table_name}_idx_title_days` (`title`,`days`),
	CONSTRAINT `{table_name}_viewer_id__fk_{reference}_uuid` FOREIGN KEY (`viewer_id`) REFERENCES `{reference}` (`uuid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8""".format(table_name=core_view_event_flat_table_name,
                                                reference=core_viewers_table_name)

    #write_df_to_aurora(logger, core_view_event_flat, core_view_event_flat_table_name, aurora_driver, core_view_event_flat_schema)

    # ---|| CORE VIEWER TYPE ||---#
    logger.info("""Computing CORE_VIEWER_TYPE...""")
    en_tmp = core_view_event.filter(F.col("view_type") == 'e')
    stackable_en = core_viewers \
        .join(en_tmp, core_viewers.uuid == en_tmp.viewer_id, 'left') \
        .withColumn("7", F.when(F.col("days") <= F.lit(7), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("14", F.when(F.col("days") <= F.lit(14), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("30", F.when(F.col("days") <= F.lit(30), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("90", F.when(F.col("days") <= F.lit(90), F.lit(1)).otherwise(F.lit(0))) \
        .groupBy("uuid") \
        .agg(F.max(F.col("7")).alias("7"),
             F.max(F.col("14")).alias("14"),
             F.max(F.col("30")).alias("30"),
             F.max(F.col("90")).alias("90"))

    stacked_viewer_type_en = get_stacked_viewer_type(spark, stackable_en, 'stck_en', 'e') \
        .withColumnRenamed("uuid", "uuid_en") \
        .withColumnRenamed("days", "days_en")

    kin_tmp = core_view_event.filter(F.col("view_type") == 'k')
    stackable_kin = core_viewers \
        .join(kin_tmp, core_viewers.uuid == kin_tmp.viewer_id, 'left') \
        .withColumn("7", F.when(F.col("days") <= F.lit(7), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("14", F.when(F.col("days") <= F.lit(14), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("30", F.when(F.col("days") <= F.lit(30), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("90", F.when(F.col("days") <= F.lit(90), F.lit(1)).otherwise(F.lit(0))) \
        .groupBy("uuid") \
        .agg(F.max(F.col("7")).alias("7"),
             F.max(F.col("14")).alias("14"),
             F.max(F.col("30")).alias("30"),
             F.max(F.col("90")).alias("90"))

    stacked_viewer_type_kin = get_stacked_viewer_type(spark, stackable_kin, 'stck_kin', 'k') \
        .withColumnRenamed("uuid", "uuid_kin") \
        .withColumnRenamed("days", "days_kin")

    stackable_dmp = core_viewers \
        .join(core_dmp_event, core_viewers.uuid == core_dmp_event.viewer_id, 'left') \
        .withColumn("daysBetween", F.datediff(F.to_date(F.lit(to_date), "yyyy-MM-dd"), F.col("date"))) \
        .withColumn("days", F.when(F.col("daysBetween").isNull(), F.col("daysBetween"))
                    .when(F.col("daysBetween") <= F.lit(7), F.lit(7))
                    .when((F.col("daysBetween") > F.lit(7)) & (F.col("daysBetween") <= F.lit(14)), F.lit(14))
                    .when((F.col("daysBetween") > F.lit(14)) & (F.col("daysBetween") <= F.lit(30)), F.lit(30))
                    .otherwise(F.lit(90))) \
        .withColumn("7", F.when(F.col("days") <= F.lit(7), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("14", F.when(F.col("days") <= F.lit(14), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("30", F.when(F.col("days") <= F.lit(30), F.lit(1)).otherwise(F.lit(0))) \
        .withColumn("90", F.when(F.col("days") <= F.lit(90), F.lit(1)).otherwise(F.lit(0))) \
        .groupBy("uuid") \
        .agg(F.max(F.col("7")).alias("7"),
             F.max(F.col("14")).alias("14"),
             F.max(F.col("30")).alias("30"),
             F.max(F.col("90")).alias("90"))

    stacked_viewer_type_dmp = get_stacked_viewer_type(spark, stackable_dmp, 'stck_dmp', 'd') \
        .withColumnRenamed("uuid", "uuid_dmp") \
        .withColumnRenamed("days", "days_dmp")

    join_condition_1 = [
        stacked_viewer_type_kin.uuid_kin == stacked_viewer_type_dmp.uuid_dmp,
        stacked_viewer_type_kin.days_kin == stacked_viewer_type_dmp.days_dmp
    ]

    core_viewer_type_tmp = stacked_viewer_type_kin \
        .join(stacked_viewer_type_dmp, join_condition_1) \
        .drop(stacked_viewer_type_dmp.uuid_dmp) \
        .drop(stacked_viewer_type_dmp.days_dmp)

    join_condition_2 = [
        core_viewer_type_tmp.uuid_kin == stacked_viewer_type_en.uuid_en,
        core_viewer_type_tmp.days_kin == stacked_viewer_type_en.days_en
    ]

    core_viewer_type = core_viewer_type_tmp \
        .join(stacked_viewer_type_en, join_condition_2) \
        .drop(stacked_viewer_type_en.uuid_en) \
        .drop(stacked_viewer_type_en.days_en) \
        .withColumn("id", F.lit(0)) \
        .withColumnRenamed("uuid_kin", "viewer_id") \
        .withColumnRenamed("days_kin", "days") \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id") \
        .orderBy("viewer_id", "days")

    core_viewer_type_partitions = core_viewer_type.rdd.getNumPartitions()
    logger.info(f"""CORE_VIEWER_TYPE: {core_viewer_type_partitions} partitions""")

    core_viewer_type_table_name = 'core_viewertype_100'
    core_viewer_type_schema = """CREATE TABLE `{table_name}` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`days` int(11) NOT NULL,
	`has_view_type_k` tinyint(1) DEFAULT NULL,
	`has_view_type_d` tinyint(1) DEFAULT NULL,
	`has_view_type_e` tinyint(1) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	PRIMARY KEY (`id`),
    KEY `{table_name}_has_view_type_k_cb530154` (`has_view_type_k`),
    KEY `{table_name}_has_view_type_d_9e97fec2` (`has_view_type_d`),
    KEY `{table_name}_has_view_type_e_74495293` (`has_view_type_e`),
    KEY `{table_name}_days_3a2f23b1` (`days`),
    KEY `{reference}_viewer__73944d_idx` (`viewer_id`,`days`),
    KEY `{reference}_days_d95b3e_idx` (`days`,`has_view_type_k`),
    KEY `{reference}_days_df0752_idx` (`days`,`has_view_type_e`),
    KEY `{reference}_days_3e93ac_idx` (`days`,`has_view_type_d`),
    KEY `{reference}_days_a2e45e_idx` (`days`,`has_view_type_e`,`has_view_type_k`),
    KEY `{reference}_days_3fc90e_idx` (`days`,`has_view_type_k`,`has_view_type_d`),
    KEY `{reference}_days_b29e06_idx` (`days`,`has_view_type_d`,`has_view_type_e`),
    KEY `{reference}_days_d09e39_idx` (`days`,`has_view_type_d`,`has_view_type_e`,`has_view_type_k`),
    KEY `{table_name}_idx_has_e_has_d_has_k` (`has_view_type_e`,`has_view_type_d`,`has_view_type_k`),
    KEY `{table_name}_idx_has_e_has_k` (`has_view_type_e`,`has_view_type_k`),
    KEY `{table_name}_idx_has_e_has_d` (`has_view_type_e`,`has_view_type_d`),
    KEY `{table_name}_idx_has_d_has_k` (`has_view_type_d`,`has_view_type_k`),
    KEY `{table_name}_idx_has_d_days` (`has_view_type_d`,`days`),
    KEY `{table_name}_idx_has_e_days` (`has_view_type_e`,`days`),
    KEY `{table_name}_idx_has_k_days` (`has_view_type_k`,`days`),
    KEY `{table_name}_idx_days_has_k_viewer` (`days`,`has_view_type_k`,`viewer_id`),
    KEY `{table_name}_idx_days_has_d_has_k_viewer` (`days`,`has_view_type_d`,`has_view_type_k`,`viewer_id`),
    KEY `{table_name}_idx_days_has_e_has_k_viewer` (`days`,`has_view_type_e`,`has_view_type_k`,`viewer_id`),
    KEY `{table_name}_idx_days_has_e_viewer` (`days`,`has_view_type_e`,`viewer_id`),
    KEY `{table_name}_idx_days_has_e_has_d_viewer` (`days`,`has_view_type_e`,`has_view_type_d`,`viewer_id`),
    KEY `{table_name}_idx_days_has_e_has_d_has_k_viewer` (`days`,`has_view_type_e`,`has_view_type_d`,`has_view_type_k`,`viewer_id`),
    KEY `{table_name}_idx_days_has_d_viewer` (`days`,`has_view_type_d`,`viewer_id`),
    KEY `{table_name}_idx_days_has_k_viewer_id` (`days`,`has_view_type_k`,`viewer_id`),
	CONSTRAINT `{table_name}_viewer_id__fk_{reference}_uuid` FOREIGN KEY (`viewer_id`) REFERENCES `{reference}` (`uuid`)
	) ENGINE=InnoDB DEFAULT CHARSET=utf8""".format(table_name=core_viewer_type_table_name,
                                                reference=core_viewers_table_name)

    #write_df_to_aurora(logger, core_viewer_type, core_viewer_type_table_name, aurora_driver, core_viewer_type_schema)

    logger.info("Andromeda: COMPLETED BASE SUCCESSFULLY!")
    alert.send_alert("Andromeda: COMPLETED BASE SUCCESSFULLY!")

    # OPTIMIZATION SECTION
    logger.info("Andromeda: STARTING SEQUENCE OPTIMIZATION!")
    alert.send_alert("Andromeda: STARTING SEQUENCE OPTIMIZATION!")


    # VIEWS:

    # core_view_k = create_view(_types=['k'])
    # core_view_d = create_view(_types=['d'])
    # core_view_e = create_view(_types=['e'])

    # core_view_kd_and = create_view(_types=['k', 'd'], _logic="AND")
    # core_view_kd_or = create_view(_types=['k', 'd'], _logic="OR")

    # core_view_ke_and = create_view(_types=['k', 'e'], _logic="AND")
    # core_view_ke_or = create_view(_types=['k', 'e'], _logic="OR")

    # core_view_de_and = create_view(_types=['d', 'e'], _logic="AND")
    # core_view_de_or = create_view(_types=['d', 'e'], _logic="OR")

    # core_view_kde_and = create_view(_types=['k', 'd', 'e'], _logic="AND")
    # core_view_kde_or = create_view(_types=['k', 'd', 'e'], _logic="OR")

    #####################
    #    VIEWERS DF     #
    #####################

    # .withColumnRenamed("days", "view_days") \

    core_viewer_type_k = core_viewer_type \
        .filter(core_viewer_type.has_view_type_k == 1) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_d = core_viewer_type \
        .filter(core_viewer_type.has_view_type_d == 1) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_e = core_viewer_type \
        .filter(core_viewer_type.has_view_type_e == 1) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_kd = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_d == 1)) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_ke = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_de = core_viewer_type \
        .filter((core_viewer_type.has_view_type_d == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    core_viewer_type_kde = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_d == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .select("id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e", "viewer_id")

    #####################
    # VIEWERSACTIVITIES #
    #####################

    # core_aux_viewersactivities_k_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_k U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_k_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_k, core_view_event.viewer_id == core_viewer_type_k.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_k.days.alias("view_days"))

    core_aux_viewersactivities_k_table_name = 'core_aux_viewersactivities_k'
    core_aux_viewersactivities_k_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_k_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_k_df, core_aux_viewersactivities_k_table_name, aurora_driver, core_aux_viewersactivities_k_schema)

    # core_aux_viewersactivities_d_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_d U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_d_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_d, core_view_event.viewer_id == core_viewer_type_d.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_d.days.alias("view_days"))

    core_aux_viewersactivities_d_table_name = 'core_aux_viewersactivities_d'
    core_aux_viewersactivities_d_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_d_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_d_df, core_aux_viewersactivities_d_table_name, aurora_driver, core_aux_viewersactivities_d_schema)

    # core_aux_viewersactivities_e_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_e U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_e_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_e, core_view_event.viewer_id == core_viewer_type_e.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_e.days.alias("view_days"))

    core_aux_viewersactivities_e_table_name = 'core_aux_viewersactivities_e'
    core_aux_viewersactivities_e_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_e_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_e_df, core_aux_viewersactivities_e_table_name, aurora_driver, core_aux_viewersactivities_e_schema)

    # core_aux_viewersactivities_kd_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_kd_and U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_kd_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_kd, core_view_event.viewer_id == core_viewer_type_kd.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_kd.days.alias("view_days"))

    core_aux_viewersactivities_kd_table_name = 'core_aux_viewersactivities_kd'
    core_aux_viewersactivities_kd_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_kd_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_kd_df, core_aux_viewersactivities_kd_table_name, aurora_driver, core_aux_viewersactivities_kd_schema)

    # core_aux_viewersactivities_ke_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_ke_and U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_ke_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_ke, core_view_event.viewer_id == core_viewer_type_ke.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_ke.days.alias("view_days"))

    core_aux_viewersactivities_ke_table_name = 'core_aux_viewersactivities_ke'
    core_aux_viewersactivities_ke_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_ke_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_ke_df, core_aux_viewersactivities_ke_table_name, aurora_driver, core_aux_viewersactivities_ke_schema)

    # core_aux_viewersactivities_de_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_de_and U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_de_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_de, core_view_event.viewer_id == core_viewer_type_de.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_de.days.alias("view_days"))

    core_aux_viewersactivities_de_table_name = 'core_aux_viewersactivities_de'
    core_aux_viewersactivities_de_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_de_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_de_df, core_aux_viewersactivities_de_table_name, aurora_driver, core_aux_viewersactivities_de_schema)

    # core_aux_viewersactivities_kde_df = spark.sql("""
    #     SELECT
    #         core_viewevent_100.id as _id,
    #         core_viewevent_100.date,
    #         core_viewevent_100.weekday,
    #         core_viewevent_100.view_type,
    #         core_viewevent_100.channel,
    #         core_viewevent_100.title,
    #         core_viewevent_100.device_type,
    #         core_viewevent_100.watch_category,
    #         core_viewevent_100.watch_time,
    #         core_viewevent_100.viewer_id,
    #         core_viewevent_100.os_name,
    #         core_viewevent_100.fruition_type,
    #         core_viewevent_100.play_type,
    #         core_viewevent_100.days,
    #         U0.days as view_days
    #     FROM
    #         core_viewevent_100
    #     JOIN
    #         core_view_kde_and U0 ON core_viewevent_100.viewer_id = U0.viewer_id
    #     """)

    core_aux_viewersactivities_kde_df = core_view_event \
        .withColumnRenamed("id", "_id") \
        .join(core_viewer_type_kde, core_view_event.viewer_id == core_viewer_type_kde.viewer_id, 'inner') \
        .select("date", "weekday", "view_type", "channel", "title", "device_type", "watch_category",
                "watch_time", core_view_event.viewer_id, "os_name", "fruition_type", "play_type", core_view_event.days, core_viewer_type_kde.days.alias("view_days"))

    core_aux_viewersactivities_kde_table_name = 'core_aux_viewersactivities_kde'
    core_aux_viewersactivities_kde_schema = """
	CREATE TABLE `{table_name}` (
	`_id` int(11) NOT NULL AUTO_INCREMENT,
	`date` date NOT NULL,
	`weekday` varchar(50) DEFAULT NULL,
	`view_type` varchar(50) DEFAULT NULL,
	`channel` varchar(100) DEFAULT NULL,
	`title` varchar(200) DEFAULT NULL,
	`device_type` varchar(100) DEFAULT NULL,
	`watch_category` varchar(200) DEFAULT NULL,
	`watch_time` bigint(20) DEFAULT NULL,
	`viewer_id` varchar(200) NOT NULL,
	`os_name` varchar(100) DEFAULT NULL,
	`fruition_type` varchar(50) DEFAULT NULL,
	`play_type` varchar(50) DEFAULT NULL,
	`days` int(11) NOT NULL,
	`view_days` int(11) NOT NULL,
	KEY `{table_name}_idx_view_days_days` (`view_days`,`days`),
    KEY `{table_name}_idx_view_days_date` (`view_days`,`date`),
	KEY `{table_name}_idx_days_view_days` (`days`,`view_days`),
	KEY `{table_name}_idx_days_view_days_channel` (`days`,`view_days`,`channel`)
	)
	""".format(table_name=core_aux_viewersactivities_kde_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_kde_df, core_aux_viewersactivities_kde_table_name, aurora_driver, core_aux_viewersactivities_kde_schema)

    #############
    # WATCHTIME #
    #############

    # core_aux_watchtime_k_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_k U0)
    #     """)

    core_aux_watchtime_k_df = core_view_event_flat \
        .join(core_viewer_type_k, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_k_table_name = 'core_aux_watchtime_k'
    core_aux_watchtime_k_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_k_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_k_df, core_aux_watchtime_k_table_name, aurora_driver, core_aux_watchtime_k_schema)

    # core_aux_watchtime_d_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_d U0)
    #     """)

    core_aux_watchtime_d_df = core_view_event_flat \
        .join(core_viewer_type_d, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_d_table_name = 'core_aux_watchtime_d'
    core_aux_watchtime_d_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_d_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_d_df, core_aux_watchtime_d_table_name, aurora_driver, core_aux_watchtime_d_schema)

    # core_aux_watchtime_e_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_e U0)
    #     """)

    core_aux_watchtime_e_df = core_view_event_flat \
        .join(core_viewer_type_e, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_e_table_name = 'core_aux_watchtime_e'
    core_aux_watchtime_e_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_e_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_e_df, core_aux_watchtime_e_table_name, aurora_driver, core_aux_watchtime_e_schema)

    # core_aux_watchtime_kd_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_kd U0)
    #     """)

    core_aux_watchtime_kd_df = core_view_event_flat \
        .join(core_viewer_type_kd, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_kd_table_name = 'core_aux_watchtime_kd'
    core_aux_watchtime_kd_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_kd_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_kd_df, core_aux_watchtime_kd_table_name, aurora_driver, core_aux_watchtime_kd_schema)

    # core_aux_watchtime_ke_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_ke U0)
    #     """)

    core_aux_watchtime_ke_df = core_view_event_flat \
        .join(core_viewer_type_ke, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_ke_table_name = 'core_aux_watchtime_ke'
    core_aux_watchtime_ke_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_ke_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_ke_df, core_aux_watchtime_ke_table_name, aurora_driver, core_aux_watchtime_ke_schema)

    # core_aux_watchtime_de_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_de U0)
    #     """)

    core_aux_watchtime_de_df = core_view_event_flat \
        .join(core_viewer_type_de, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_de_table_name = 'core_aux_watchtime_de'
    core_aux_watchtime_de_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_de_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_de_df, core_aux_watchtime_de_table_name, aurora_driver, core_aux_watchtime_de_schema)

    # core_aux_watchtime_kde_df = spark.sql("""
    #     SELECT
    #         core_vieweventflat_100.id,
    #         core_vieweventflat_100.view_type,
    #         core_vieweventflat_100.channel,
    #         core_vieweventflat_100.title,
    #         core_vieweventflat_100.device_type,
    #         core_vieweventflat_100.watch_category,
    #         core_vieweventflat_100.watch_time,
    #         core_vieweventflat_100.os_name,
    #         core_vieweventflat_100.gender,
    #         core_vieweventflat_100.age,
    #         core_vieweventflat_100.province,
    #         core_vieweventflat_100.count_events,any
    #         core_vieweventflat_100.viewer_id,
    #         core_vieweventflat_100.days,
    #         core_vieweventflat_100.fruition_type,
    #         core_vieweventflat_100.play_type
    #     FROM
    #         core_vieweventflat_100
    #     WHERE
    #         core_vieweventflat_100.viewer_id IN (
    #         SELECT
    #             U0.viewer_id
    #         FROM
    #             core_view_kde U0)
    #     """)

    core_aux_watchtime_kde_df = core_view_event_flat \
        .join(core_viewer_type_kde, ['viewer_id'], 'leftsemi') \
        .select("view_type", "channel", "title", "device_type", "watch_category", "watch_time", "os_name",
                "gender", "age", "province", "count_events", "viewer_id", "days", "fruition_type", "play_type")

    core_aux_watchtime_kde_table_name = 'core_aux_watchtime_kde'
    core_aux_watchtime_kde_schema = """
    CREATE TABLE `{table_name}` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `view_type` varchar(50) DEFAULT NULL,
    `channel` varchar(100) DEFAULT NULL,
    `title` varchar(200) DEFAULT NULL,
    `device_type` varchar(100) DEFAULT NULL,
    `watch_category` varchar(200) DEFAULT NULL,
    `watch_time` bigint(20) DEFAULT NULL,
    `os_name` varchar(100) DEFAULT NULL,
    `gender` varchar(50) DEFAULT NULL,
    `age` int(11) DEFAULT NULL,
    `province` varchar(200) DEFAULT NULL,
    `count_events` int(11) NOT NULL,
    `viewer_id` varchar(200) NOT NULL,
    `days` int(11) NOT NULL,
    `fruition_type` varchar(50) DEFAULT NULL,
    `play_type` varchar(50) DEFAULT NULL,
    KEY `{table_name}_idx_viewer_id_fk_core_viewer_uuid` (`viewer_id`),
    KEY `{table_name}_idx_view_type` (`view_type`),
    KEY `{table_name}_idx_channel` (`channel`),
    KEY `{table_name}_idx_title` (`title`),
    KEY `{table_name}_idx_device_type` (`device_type`),
    KEY `{table_name}_idx_watch_category` (`watch_category`),
    KEY `{table_name}_idx_os_name` (`os_name`),
    KEY `{table_name}_idx_gender` (`gender`),
    KEY `{table_name}_idx_age` (`age`),
    KEY `{table_name}_idx_province` (`province`),
    KEY `{table_name}_idx_days` (`days`),
    KEY `{table_name}_idx_fruition_type` (`fruition_type`),
    KEY `{table_name}_idx_play_type` (`play_type`),
    KEY `{table_name}_idx_days_channel` (`days`,`channel`),
    KEY `{table_name}_idx_days_channel_viewer_id` (`days`,`channel`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_viewer_id` (`watch_category`,`viewer_id`),
    KEY `{table_name}_idx_title_viewer_id` (`title`,`viewer_id`),
    KEY `{table_name}_idx_watch_category_title_viewer_id` (`watch_category`,`title`,`viewer_id`),
    KEY `{table_name}_idx_gender_days_age` (`gender`,`days`,`age`),
    KEY `{table_name}_idx_gender_age_watchtime_pt_ft` (`gender`,`age`,`watch_time`,`play_type`,`fruition_type`),
    KEY `{table_name}_idx_gender_age_watchtime` (`gender`,`age`,`watch_time`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_watchtime_kde_table_name)

    #write_df_to_aurora(logger, core_aux_watchtime_kde_df, core_aux_watchtime_kde_table_name, aurora_driver, core_aux_watchtime_kde_schema)

    ###################
    # TOTALVIEWSDELTA #
    ###################

    # core_aux_totalviewsdelta_df = spark.sql("""
    #     SELECT `core_viewevent_100`.`view_type`,
    #         `core_viewevent_100`.`date`,
    #         COUNT(`core_viewevent_100`.`view_type`) AS `count`
    #     FROM `core_viewevent_100`
    #     GROUP BY `core_viewevent_100`.`view_type`, `core_viewevent_100`.`date`
    #     """)

    core_aux_totalviewsdelta_df = core_view_event \
        .groupBy("view_type", "date") \
        .agg(F.count("view_type").alias("count")) \
        .select("view_type", "date", "count")

    core_aux_totalviewsdelta_table_name = 'core_aux_totalviewsdelta'
    core_aux_totalviewsdelta_schema = """
    CREATE TABLE `{table_name}` (
    `view_type` varchar(100) NOT NULL,
    `date` date NOT NULL DEFAULT '0000-00-00',
    `count` bigint(20) NOT NULL,
    PRIMARY KEY (`view_type`,`date`)
    ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_totalviewsdelta_table_name)

    #write_df_to_aurora(logger, core_aux_totalviewsdelta_df, core_aux_totalviewsdelta_table_name, aurora_driver, core_aux_totalviewsdelta_schema)

    ########
    # VENN #
    ########

    # core_aux_venn_df = spark.sql("""
    #     SELECT ""total"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_d = 1 OR `core_viewertype_100`.has_view_type_e = 1 OR `core_viewertype_100`.has_view_type_k
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""k"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_k = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""d"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_d = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""e"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_e = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""kd"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_k = 1 AND `core_viewertype_100`.has_view_type_d = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""ke"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_k = 1 AND `core_viewertype_100`.has_view_type_e = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""de"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`l
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_d = 1 AND `core_viewertype_100`.has_view_type_e = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     UNION
    #     SELECT ""kde"" as view_type, COUNT(*) as `count`, `core_viewertype_100`.`days`
    #     FROM `core_viewertype_100`
    #     WHERE `core_viewertype_100`.has_view_type_k = 1 AND `core_viewertype_100`.has_view_type_d = 1 AND `core_viewertype_100`.has_view_type_e = 1
    #     GROUP BY `core_viewertype_100`.`days`
    #     """)

    core_aux_venn_total = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) | (core_viewer_type.has_view_type_d == 1) | (core_viewer_type.has_view_type_e == 1)) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("total")) \
        .select("view_type", "count", "days")

    core_aux_venn_k = core_viewer_type \
        .filter(core_viewer_type.has_view_type_k == 1) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("k")) \
        .select("view_type", "count", "days")

    core_aux_venn_d = core_viewer_type \
        .filter(core_viewer_type.has_view_type_d == 1) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("d")) \
        .select("view_type", "count", "days")

    core_aux_venn_e = core_viewer_type \
        .filter(core_viewer_type.has_view_type_e == 1) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("e")) \
        .select("view_type", "count", "days")

    core_aux_venn_kd = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_d == 1)) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("kd")) \
        .select("view_type", "count", "days")

    core_aux_venn_ke = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("ke")) \
        .select("view_type", "count", "days")

    core_aux_venn_de = core_viewer_type \
        .filter((core_viewer_type.has_view_type_d == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("de")) \
        .select("view_type", "count", "days")

    core_aux_venn_kde = core_viewer_type \
        .filter((core_viewer_type.has_view_type_k == 1) & (core_viewer_type.has_view_type_d == 1) & (core_viewer_type.has_view_type_e == 1)) \
        .groupBy("days") \
        .agg(F.count("*").alias("count")) \
        .withColumn("view_type", lit("kde")) \
        .select("view_type", "count", "days")

    core_aux_venn_df = core_aux_venn_total \
        .union(core_aux_venn_k) \
        .union(core_aux_venn_d) \
        .union(core_aux_venn_e) \
        .union(core_aux_venn_kd) \
        .union(core_aux_venn_ke) \
        .union(core_aux_venn_de) \
        .union(core_aux_venn_kde)

    core_aux_venn_table_name = 'core_aux_venn'
    core_aux_venn_schema = """
        CREATE TABLE `{table_name}` (
        `view_type` varchar(100) NOT NULL,
        `count` bigint(20) DEFAULT NULL,
        `days` int(11) NOT NULL,
        PRIMARY KEY (`days`,`view_type`),
        KEY `{table_name}_days_IDX` (`days`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name=core_aux_venn_table_name)

    #write_df_to_aurora(logger, core_aux_venn_df, core_aux_venn_table_name, aurora_driver, core_aux_venn_schema)

    #####################
    # REGISTRATIONSVIEW #
    #####################

    # core_aux_venn_df = spark.sql("""
    #     SELECT
    #     core_viewer_100.uuid ,
    #     core_viewer_100.gender ,
    #     core_viewer_100.age ,
    #     core_viewer_100.province ,
    #     core_viewer_100.registration_source ,
    #     core_viewer_100.login_provider ,
    #     core_viewer_100.registration_date ,
    #     core_viewertype_100.days,
    #     core_viewertype_100.has_view_type_k,
    #     core_viewertype_100.has_view_type_d,
    #     core_viewertype_100.has_view_type_e
    #     FROM
    #     core_viewer_100
    #     INNER JOIN core_viewertype_100 ON
    #     core_viewer_100.uuid = core_viewertype_100.viewer_id
    #     """)

    core_viewer_type_filtered = core_viewer_type \
        .select("viewer_id", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e")

    core_aux_venn_df = core_viewers \
        .join(core_viewer_type_filtered, core_viewers.uuid == core_viewer_type_filtered.viewer_id, 'inner') \
        .select("uuid", "gender", "age", "province", "registration_source", "login_provider",
                "registration_date", "days", "has_view_type_k", "has_view_type_d", "has_view_type_e")

    core_aux_venn_table_name = 'core_aux_registrationsview'
    core_aux_venn_schema = """
        "CREATE TABLE `{table_name}` (
        `uuid` varchar(100) NOT NULL DEFAULT '',
        `gender` varchar(100) DEFAULT NULL,
        `age` int(11) DEFAULT NULL,
        `province` varchar(100) DEFAULT NULL,
        `registration_source` varchar(100) DEFAULT NULL,
        `login_provider` varchar(100) DEFAULT NULL,
        `registration_date` date DEFAULT NULL,
        `days` int(11) DEFAULT NULL,
        `has_view_type_k` tinyint(1) DEFAULT NULL,
        `has_view_type_d` tinyint(1) DEFAULT NULL,
        `has_view_type_e` tinyint(1) DEFAULT NULL,
        KEY `{table_name}_gender_IDX` (`gender`),
        KEY `{table_name}_age_IDX` (`age`),
        KEY `{table_name}_province_IDX` (`province`),
        KEY `{table_name}_registration_source_IDX` (`registration_source`),
        KEY `{table_name}_login_provider_IDX` (`login_provider`),
        KEY `{table_name}_registration_date_IDX` (`registration_date`),
        KEY `{table_name}_days_IDX` (`days`),
        KEY `{table_name}_uuid_IDX` (`uuid`),
        KEY `{table_name}_registration_date_days_IDX` (`registration_date`,`days`),
        KEY `{table_name}_has_view_type_k_IDX` (`has_view_type_k`),
        KEY `{table_name}_has_view_type_d_IDX` (`has_view_type_d`),
        KEY `{table_name}_has_view_type_e_IDX` (`has_view_type_e`)
        )" ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name=core_aux_venn_table_name)

    #write_df_to_aurora(logger, core_aux_venn_df, core_aux_venn_table_name, aurora_driver, core_aux_venn_schema)

    ##############
    # PARAMETERS #
    ##############

    core_aux_parameters_df = spark.sql("""
        SELECT
            'last_update_andromeda',
            DATE_FORMAT(now(), '%Y-%m-%dT%TZ')
        """)

    core_aux_parameters_table_name = 'core_aux_parameters'
    core_aux_parameters_schema = """
        CREATE TABLE `{table_name}` (
        `key` varchar(200) NOT NULL,
        `value` text,
        PRIMARY KEY (`key`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
    """.format(table_name=core_aux_parameters_table_name)

    #write_df_to_aurora(logger, core_aux_parameters_df, core_aux_parameters_table_name, aurora_driver, core_aux_parameters_schema)

    ############################
    # VIEWERSACTIVITIES_ONLY_K #
    ############################

    # core_aux_viewersactivities_only_k_df = spark.sql("""
    # SELECT `core_viewevent_100`.`date`,
    #     COUNT(DISTINCT `core_viewevent_100`.`viewer_id`) AS `activites`,
    #     `core_viewevent_100`.`view_type`,
    #     `core_viewevent_100`.`days`
    # FROM `core_viewevent_100`
    # GROUP BY `core_viewevent_100`.`date`, `core_viewevent_100`.`view_type`, `core_viewevent_100`.`days`
    # ORDER BY `core_viewevent_100`.`date` ASC
    # """)

    core_aux_viewersactivities_only_k_df = core_view_event \
        .groupBy("date", "view_type", "days") \
        .agg(F.count("viewer_id").alias("activites")) \
        .select("date", "activites", "view_type", "days") \
        .orderBy("date")

    core_aux_viewersactivities_only_k_table_name = 'core_aux_viewersactivities_only_k'
    core_aux_viewersactivities_only_k_schema = """
        CREATE TABLE `{table_name}` (
        `date` date NOT NULL DEFAULT '0000-00-00',
        `activities` bigint(20) DEFAULT NULL,
        `view_type` varchar(100) NOT NULL DEFAULT '',
        `days` int(11) NOT NULL DEFAULT '0',
        PRIMARY KEY (`date`,`view_type`,`days`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name=core_aux_viewersactivities_only_k_table_name)

    #write_df_to_aurora(logger, core_aux_viewersactivities_only_k_df, core_aux_viewersactivities_only_k_table_name, aurora_driver, core_aux_viewersactivities_only_k_schema)

    ############################
    #      CORE_AUX_CACHE      #
    ############################

    core_aux_cache_df = spark.sql(
        """SELECT 'START' as `key`, null as `value`""")

    core_aux_cache_table_name = 'core_aux_cache'
    core_aux_cache_schema = """
        CREATE TABLE `{table_name}` (
            `id` bigint(20) NOT NULL AUTO_INCREMENT,
            `key` longtext NOT NULL,
            `value` longtext,
            PRIMARY KEY (`id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
        """.format(table_name=core_aux_cache_table_name)

    #write_df_to_aurora(logger, core_aux_cache_df, core_aux_cache_table_name, aurora_driver, core_aux_cache_schema)

    logger.info("Andromeda: OPTIMIZATION COMPLETED SUCCESSFULLY!")
    alert.send_alert("Andromeda: COMPLETED SUCCESSFULLY!")