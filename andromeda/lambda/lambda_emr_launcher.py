"""

Parametri che devono essere settati nella lambda come variabili d'ambiente:
    conf_bucket : str
    conf_key : str
    instance_profile_role : str
    subnet_id : str
    service_access_security_group : str
    slave_security_group : str
    master_security_group : str
    tag_bu : str

Nel bucket 'conf_bucket' al path 'conf_key' deve essere presente un JSON con i parametri di configurazione.

Parametri del file di configurazione all'interno del file :
    Obbligatori:
        cluster_name : str
        emr_release : str
        spark_script_path : str     esempio: "s3://dl-aws-emr-jobs/test/device_match.py"
        master_node_instance_type : str
        core_nodes_instance_type : str
        n_core_nodes: int
        log_path : str   esempio: "s3://dl-aws-emr-jobs/test/EMR_logs/"
    Opzionali:
        install_packages : list(str) esempio: ["boto3"]
        py_files_path : str  esempio: "s3://dl-aws-emr-jobs/test/zipped_libs.zip"
        files_path : str     esempio: "s3://dl-aws-emr-jobs/test/config_job_run.json"
        jars_path : str      esempio: "s3://dl-aws-emr-jobs/test/jarfile.jar"
        other_bootstrap_path : str


        s3_max_retries : str (contenente un int)
        dynamic_allocation : 'true' / 'false'
        maximize_resource_allocation : 'true' / 'false'

        le seguenti 4 proprietà di memoria verranno valutate solo se presenti tutte e quattro:
            driver_memory : str    esempio: "18G"
            executor_memory : str  esempio:
            num_executors : str (contenente un int)
            executor_cores : str (contenente un int)


"""

import json
import boto3
import os
import logging

CONF_BUCKET_NAME = os.environ.get('conf_bucket')
CONF_KEY_NAME = os.environ.get('conf_key')

INSTANCE_PROFILE_ROLE = os.environ.get('instance_profile_role')

MAX_NUM_CORE_NODES = 30

subnet_id = os.environ.get('subnet_id')
service_access_security_group = os.environ.get('service_access_security_group')
slave_security_group = os.environ.get('slave_security_group')
master_security_group = os.environ.get('master_security_group')

tag_bu = os.environ.get('tag_bu')

def get_s3_client():
    conn = boto3.client(service_name='s3', region_name='eu-west-1')
    return conn

def get_job_launcher_config():
    conn = get_s3_client()
    file_content = conn.get_object(Bucket=CONF_BUCKET_NAME, Key=CONF_KEY_NAME)['Body'].read().decode()
    job_launcher_config = json.loads(file_content)
    return job_launcher_config

def create_packages_bootstrap(packages_list):
    if len(packages_list) == 0:
        return ''
    bootstrap_cmd = '''\
#!/bin/bash

set -x -e
sudo python3 -m pip install'''
    for package in packages_list:
        bootstrap_cmd = f'{bootstrap_cmd} {package}'

    conn = get_s3_client()
    folder_path = '/'.join(CONF_KEY_NAME.split('/')[:-1])
    key_path = f'{folder_path}/bootstrap/packages_bootstrap.sh'
    conn.put_object(Bucket=CONF_BUCKET_NAME, Key=key_path, Body=str.encode(bootstrap_cmd))
    return f's3://{CONF_BUCKET_NAME}/{key_path}'


def build_bootstrap_action(packages, other):
    len_packages = len(packages['ScriptBootstrapAction']['Path'])
    len_other = len(other['ScriptBootstrapAction']['Path'])
    if len_packages == 0:
        if len_other == 0:
            return []
        else:
            return [other]
    else:
        if len_other == 0:
            return [packages]
        else:
            return [packages, other]



def build_configurations(config):
    s3_max_retries = config.get('s3_max_retries')
    maximize_resource_allocation = config.get('maximize_resource_allocation')
    dynamic_allocation = config.get('dynamic_allocation')
    driver_memory = config.get("driver_memory")
    executor_memory = config.get("executor_memory")
    num_executors = config.get("num_executors")
    executor_cores = config.get("executor_cores")

    configurations = [{
        'Classification': 'spark-hive-site',
        'Properties': {
            "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"},
        "Configurations": []
    }]

    if s3_max_retries:
        configurations.append({
            'Classification': 'emrfs-site',
            'Properties': {"fs.s3.maxRetries": s3_max_retries}
        })

    if maximize_resource_allocation:
        configurations.append({
            'Classification': 'spark',
            'Properties': {"maximizeResourceAllocation": maximize_resource_allocation},
        })

    valid_memory_properties = driver_memory and executor_memory and num_executors and executor_cores

    if dynamic_allocation or valid_memory_properties:
        spark_defaults = {
                            'Classification': 'spark-defaults',
                            'Properties': {}}
        if valid_memory_properties:
            memory_properties = {
                "spark.driver.memory": driver_memory,
                "spark.executor.memory": executor_memory,
                "spark.executor.instances": num_executors,
                "spark.executor.cores": executor_cores
            }
            spark_defaults['Properties'] = memory_properties

        if dynamic_allocation:
            spark_defaults['Properties']["spark.dynamicAllocation.enabled"] = dynamic_allocation

        configurations.append(spark_defaults)

    return configurations

def build_tags(config, tag_bu):
    tags =  [ { 'Key': 'Name','Value': config['cluster_name']}]
    if tag_bu:
        tags.append( {'Key': 'BU', 'Value': tag_bu } )
    return tags

def lambda_handler(event, context):
    job_launcher_config = get_job_launcher_config()

    ############### --- OBBLIGATORI --- ####################
    cluster_name = job_launcher_config['cluster_name']
    emr_release = job_launcher_config["emr_release"]

    master_node_instance_type = job_launcher_config['master_node_instance_type']
    core_nodes_instance_type = job_launcher_config['core_nodes_instance_type']
    n_core_nodes = min(MAX_NUM_CORE_NODES, int(job_launcher_config['n_core_nodes']))

    log_path = job_launcher_config["log_path"]

    ############### --- OPZIONALI --- #####################

    install_packages = job_launcher_config.get("install_packages", [])
    packages_bootstrap_path = create_packages_bootstrap(install_packages)
    packages_bootstrap_action = {
                                "Name": "packages_bootstrap_action",
                                "ScriptBootstrapAction": {
                                    "Path": packages_bootstrap_path
                                }
                            }

    other_bootstrap_path = job_launcher_config.get("other_bootstrap_path")
    other_bootstrap_action = {
        "Name": "other_bootstrap_action",
        "ScriptBootstrapAction": {
            "Path": other_bootstrap_path
        }
    }

    bootstrap_action = build_bootstrap_action(packages_bootstrap_action, other_bootstrap_action)

    configurations = build_configurations(job_launcher_config)

    tags = build_tags(job_launcher_config, tag_bu)



    command_runner_arguments = ['spark-submit']

    spark_script_path = job_launcher_config.get("spark_script_path")
    py_files_path = job_launcher_config.get("py_files_path")
    files_path = job_launcher_config.get("files_path")
    jars_path = job_launcher_config.get("jars_path")

    if jars_path:
        command_runner_arguments += ['--jars', jars_path]
    if py_files_path:
        command_runner_arguments += ['--py-files', py_files_path]
    if files_path:
        command_runner_arguments += ['--files', files_path]

    command_runner_arguments += ['--deploy-mode', 'cluster',
                                 '--master', 'yarn', spark_script_path]

    if job_launcher_config.get('additional_python_arguments'):
        command_runner_arguments += job_launcher_config['additional_python_arguments']

    print(command_runner_arguments)

    conn = boto3.client(
        service_name='emr',
        region_name='eu-west-1',
    )
    cluster_id = conn.run_job_flow(
        Name=cluster_name,
        Applications=[{
            'Name': 'Spark'
        }],
        ServiceRole='EMR_DefaultRole',
        JobFlowRole=INSTANCE_PROFILE_ROLE,
        AutoScalingRole='EMR_AutoScaling_DefaultRole',
        VisibleToAllUsers=True,
        LogUri=log_path,
        ReleaseLabel=emr_release,
        Instances={
            'InstanceGroups': [
                {
                    'Name': 'Master node',
                    'Market': 'SPOT',
                    'InstanceRole': 'MASTER',
                    'InstanceType': master_node_instance_type,
                    'InstanceCount': 1,
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'st1',
                                    'SizeInGB': 500
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                        'EbsOptimized': True
                    }
                },
                {
                    'Name': 'Core nodes',
                    'Market': 'SPOT',
                    'InstanceRole': 'CORE',
                    'InstanceType': core_nodes_instance_type,
                    'InstanceCount': n_core_nodes,
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'st1',
                                    'SizeInGB': 500
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                        'EbsOptimized': True
                    }
                }
            ],
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False,
            'Ec2SubnetId': subnet_id,
            'ServiceAccessSecurityGroup': service_access_security_group,
            'EmrManagedSlaveSecurityGroup': slave_security_group,
            'EmrManagedMasterSecurityGroup': master_security_group
        },

        BootstrapActions=bootstrap_action,
        Configurations= configurations,
        Steps=[{
            'Name': 'Spark Step',
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': command_runner_arguments
            }
        }],
        ScaleDownBehavior='TERMINATE_AT_TASK_COMPLETION',
        Tags= tags
    )
    return "Started cluster {}".format(cluster_id)