import datetime
import pymsteams


class Alert:

	def __init__(self, logger, webhooks, send_alert):
		self.logger = logger
		self.teams_webhooks = webhooks
		self.enabled = send_alert

	def ms_teams_connector(self, msg, hooks):
		for hook in hooks:
			teams_connector = pymsteams.connectorcard(hook)
			teams_connector.text(msg)
			teams_connector.send()

	def send_alert(self, msg):
		if self.enabled:
			self.ms_teams_connector(msg, self.teams_webhooks)

	def send_exception_alert(self, job_name, performing_description, exception):
		msg_alert = "{job_name} - {ts} - COMPUTATION ERROR while performing: {desc}\n\n{exc}".format(job_name=job_name,
		                                                                                             desc=performing_description,
		                                                                                             exc=exception,
		                                                                                             ts=(
			                                                                                             datetime.datetime.now()))

		self.logger.info(msg_alert)
		self.send_alert(msg_alert)

	def send_no_data_alert(self, job_name, msg):
		msg_alert = "{job_name} - {ts} - NO DATA: {msg}".format(job_name=job_name,
		                                                        msg=msg,
		                                                        ts=(datetime.datetime.now()))
		self.logger.info(msg_alert)
		self.send_alert(msg_alert)
