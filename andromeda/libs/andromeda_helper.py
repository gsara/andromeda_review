import logging
from pyspark.sql import functions as F
from pyspark.sql.types import StructType
from pyspark.sql.types import StructField
from pyspark.sql.types import StringType, IntegerType, DateType
from pyspark.sql.utils import AnalysisException
import datetime


def get_logger(log_level):
    logger = logging.getLogger(__file__)
    logger.setLevel(getattr(logging, log_level.upper()))

    # create console handler with a higher log level
    ch = logging.StreamHandler()
    ch.setLevel(getattr(logging, log_level.upper()))
    # create formatter and add it to the handlers
    formatter = logging.Formatter(
        '%(asctime)s - %(levelname)s - %(message)s'
    )
    ch.setFormatter(formatter)
    # add the handlers to logger
    logger.addHandler(ch)
    return logger


def get_logged_enabler_vod_fruitions(spark, from_date, to_date):
    df_vod = spark.sql(f"""SELECT deviceid,
                                 userid, 
                                 idoggetto as codiceprodotto,
                                 concat(y,'-',m,'-',d) as tr_date, 
                                 'vod' as fruition_type, 
                                 idoggetto as video_id, 
                                 (CAST(dett AS INTEGER)/1000) as sec_dur,
                                 ts as end_ts
                          FROM dl_mediaset.dl_enabler_device_state 
                          WHERE CONCAT(y,'-',m,'-',d) BETWEEN '{from_date}' AND '{to_date}' AND
                          userid IS NOT NULL AND 
                          actionapp='V' AND 
                          dett IS NOT NULL AND 
                          tipoazione IN ('F', 'M', 'R', 'C')""") \
        .withColumn("sec_dur", F.round(F.col("sec_dur"), 0).cast(IntegerType())) \
        .withColumn("weekday", F.date_format("tr_date", 'E')) \
        .withColumn("end_ts", F.to_timestamp("end_ts", "yyyyMMddHHmmss")) \
        .withColumn("ini_ts", F.to_timestamp(F.unix_timestamp('end_ts', 'yyyyMMddHHmmss') - F.col("sec_dur"))) \
        .withColumn("rete", F.lit(None).cast(IntegerType())) \
        .select("deviceid", "userid", "tr_date", "weekday", "ini_ts", "end_ts", "sec_dur", "rete", "codiceprodotto",
                "fruition_type")

    return df_vod


def get_logged_enabler_linear_fruitions(spark, from_date, to_date):
    action_view = spark.sql(f"""SELECT deviceid AS uuid,
                                      inits AS ini_ts,
                                      endts AS end_ts,
                                      secdur AS num_sec,
                                      codicecanale,
                                      y,
                                      m,
                                      LEFT(d, 2) AS d,
                                      CONCAT(y,'-',m,'-',LEFT(d, 2)) AS std_date
                                FROM dl_mediaset.dl_enabler_action_view
                                WHERE CONCAT(y,'-',m,'-',LEFT(d, 2)) BETWEEN '{from_date}' AND '{to_date}'""")

    channels = spark.sql("""SELECT codicecanale, nomecanale as name
                            FROM dl_mediaset.dl_enabler_canali""")\
        .distinct()

    action_view_channels = action_view \
        .join(channels, action_view.codicecanale == channels.codicecanale, 'left') \
        .withColumn("ini_ts_str", F.col("ini_ts").cast(StringType())) \
        .withColumn("ini_ts", F.to_timestamp(F.col("ini_ts"), 'yyyyMMddHHmmss')) \
        .withColumn("end_ts", F.to_timestamp(F.col("end_ts"), 'yyyyMMddHHmmss')) \
        .drop(channels.codicecanale) \
        .withColumn("codicecanale", F.when(F.col("codicecanale") == 'C5HD', F.lit('C5'))
                    .when(F.col("codicecanale") == 'I1HD', F.lit('I1'))
                    .when(F.col("codicecanale") == 'R4HD', F.lit('R4'))
                    .when(F.col("codicecanale") == 'LBHD', F.lit('LB')).otherwise(F.col("codicecanale"))) \
        .select("uuid",
                F.col("codicecanale").alias("code"),
                F.col("name").alias("channel"),
                F.concat(F.substring(F.col("ini_ts_str"), 1, 4), F.lit('-'), F.substring(F.col("ini_ts_str"), 5, 2),
                         F.lit('-'), F.substring(F.col("ini_ts_str"), 7, 2)).alias("tr_date"),
                "ini_ts",
                "end_ts",
                "num_sec",
                "y",
                "m",
                "d")

    video_content_history = spark.sql(f"""SELECT orainizioeffettiva,
                                                rete,
                                                orafineeffettiva,
                                                codiceprodotto,
                                                y,
                                                m,
                                                LEFT(d,2) AS d
                                          FROM dl_mediaset.dl_vc_video_content_history
                                          WHERE tipooggetto IN ('M', 'N', 'TR', '21', '22', '23', '25', '9') AND
                                          CONCAT(y,'-',m,'-',LEFT(d, 2)) BETWEEN '{from_date}' AND '{to_date}'""")

    check_when_mode = 0
    if from_date >= "2021-01-11":
        hours_interval = '0'
    elif to_date < "2021-01-11":
        hours_interval = '2'
    else:
        check_when_mode = 1

    date_time_obj = datetime.datetime.strptime("2021-01-11", '%Y-%m-%d')

    date_limit = date_time_obj.date()

    if check_when_mode == 0:
        video_content_enriched = video_content_history \
            .withColumn("ini_ts_prog",
                        F.to_timestamp(F.col("orainizioeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL '+hours_interval+' HOURS')) \
            .withColumn("end_ts_prog",
                        F.to_timestamp(F.col("orafineeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL '+hours_interval+' HOURS')) \
            .withColumn("tr_date", F.substring(F.col("ini_ts_prog").cast(StringType()), 1, 10)) \
            .select("tr_date", "rete", "orainizioeffettiva", "orafineeffettiva", "codiceprodotto", "ini_ts_prog", "end_ts_prog")

    elif check_when_mode == 1:
        video_content_enriched = video_content_history \
            .withColumn("tr_date", F.to_date(F.substring(F.col("orainizioeffettiva").cast(StringType()), 1, 10)))\
            .withColumn("ini_ts_prog",
                        F.when(F.col("tr_date") < F.lit(date_limit), F.to_timestamp(
                            F.col("orainizioeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL 2 HOURS'))
                        .when(F.col("tr_date") >= F.lit(date_limit), F.to_timestamp(F.col("orainizioeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL 0 HOURS')))\
            .withColumn("end_ts_prog",
                        F.when(F.col("tr_date") < F.lit(date_limit), F.to_timestamp(
                            F.col("orafineeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL 2 HOURS'))
                        .when(F.col("tr_date") >= F.lit(date_limit), F.to_timestamp(F.col("orafineeffettiva"), 'yyyy-MM-dd HH:mm:ss') - F.expr('INTERVAL 0 HOURS')))\
            .select("tr_date", "rete", "orainizioeffettiva", "orafineeffettiva", "codiceprodotto", "ini_ts_prog", "end_ts_prog")

    av_join_condition = [
        action_view_channels.code == video_content_enriched.rete,
        action_view_channels.tr_date == video_content_enriched.tr_date,
        action_view_channels.ini_ts <= video_content_enriched.end_ts_prog,
        action_view_channels.end_ts >= video_content_enriched.ini_ts_prog
    ]

    if check_when_mode == 0:
        linear_fruitions = action_view_channels \
            .join(video_content_enriched, av_join_condition) \
            .drop(video_content_enriched.tr_date) \
            .withColumn("ini_prog",
                        F.when(F.col("ini_ts") < F.col("ini_ts_prog"), F.col("ini_ts_prog")).otherwise(F.col("ini_ts"))) \
            .withColumn("end_prog",
                        F.when(F.col("end_ts") > F.col("end_ts_prog"), F.col("end_ts_prog")).otherwise(F.col("end_ts"))) \
            .withColumn("ini_prog", F.col("ini_prog") + F.expr('INTERVAL '+hours_interval+' HOURS')) \
            .withColumn("end_prog", F.col("end_prog") + F.expr('INTERVAL '+hours_interval+' HOURS')) \
            .withColumn("weekday", F.date_format("tr_date", 'E')) \
            .withColumn("fruition_type", F.lit("linear")) \
            .select("uuid", "tr_date", "weekday", "fruition_type", "orainizioeffettiva", "orafineeffettiva", "ini_prog", "end_prog", "num_sec", "codiceprodotto",
                    "rete", "y", "m", "d")

    elif check_when_mode == 1:
        linear_fruitions = action_view_channels \
            .join(video_content_enriched, av_join_condition) \
            .drop(video_content_enriched.tr_date) \
            .withColumn("ini_prog",
                        F.when(F.col("ini_ts") < F.col("ini_ts_prog"), F.col("ini_ts_prog")).otherwise(F.col("ini_ts"))) \
            .withColumn("end_prog",
                        F.when(F.col("end_ts") > F.col("end_ts_prog"), F.col("end_ts_prog")).otherwise(F.col("end_ts"))) \
            .withColumn("ini_prog", F.when(F.col("tr_date") < F.lit(date_limit), F.col("ini_prog") + F.expr('INTERVAL 2 HOURS'))
                                     .when(F.col("tr_date") >= F.lit(date_limit), F.col("ini_prog") + F.expr('INTERVAL 0 HOURS')))\
            .withColumn("end_prog", F.when(F.col("tr_date") < F.lit(date_limit), F.col("end_prog") + F.expr('INTERVAL 2 HOURS'))
                                     .when(F.col("tr_date") >= F.lit(date_limit), F.col("end_prog") + F.expr('INTERVAL 0 HOURS')))\
            .withColumn("weekday", F.date_format("tr_date", 'E')) \
            .withColumn("fruition_type", F.lit("linear")) \
            .select("uuid", "tr_date", "weekday", "fruition_type", "orainizioeffettiva", "orafineeffettiva", "ini_prog", "end_prog", "num_sec", "codiceprodotto",
                    "rete", "y", "m", "d")

    userid = spark.sql(f"""SELECT CONCAT(y,'-',m,'-',d) AS tr_date,
                                 deviceId,
                                 userId
                           FROM dl_mediaset.dl_enabler_device_state
                           WHERE CONCAT(y,'-',m,'-',d) BETWEEN '{from_date}' AND '{to_date}' AND 
                           userId IS NOT NULL""") \
        .groupBy("tr_date", "deviceId") \
        .agg(F.max("userId").alias("userid"))

    uid_join_condition = [userid.tr_date == linear_fruitions.tr_date,
                          userid.deviceId == linear_fruitions.uuid]

    linear_fruitions_enriched = linear_fruitions \
        .join(userid, uid_join_condition) \
        .drop(userid.deviceId) \
        .drop(userid.tr_date) \
        .withColumnRenamed("uuid", "deviceid") \
        .withColumnRenamed("ini_prog", "ini_ts") \
        .withColumnRenamed("end_prog", "end_ts") \
        .withColumnRenamed("num_sec", "sec_dur") \
        .select("deviceid", "userid", "tr_date", "weekday", "ini_ts", "end_ts", "sec_dur", "rete", "codiceprodotto", "fruition_type")

    return linear_fruitions_enriched


def get_logged_enabler_fruitions(spark, from_date, to_date):
    linear = get_logged_enabler_linear_fruitions(spark, from_date, to_date)
    vod = get_logged_enabler_vod_fruitions(spark, from_date, to_date)
    enabler_fruitions = linear.union(vod)

    return enabler_fruitions


def get_monthly_cats(spark, partition_str):
    date_str = partition_str[2:6] + '-' + partition_str[9:11] + '-' + '01'
    try:
        monthly_cats = spark \
            .read \
            .parquet(
                "s3://dl-profile-prod/dl_rti/dl_pr_individual/UTC/monthly/{partition}".format(partition=partition_str)) \
            .filter(F.col("source") == 'gigya') \
            .withColumn("cats", F.explode(F.col("cat_list"))) \
            .withColumn("cat", F.col("cats")['cat']) \
            .withColumn("cat_sum_freq", F.col("cats")['sum_freq']) \
            .withColumn("date", F.last_day(F.to_date(F.lit(date_str), "yyyy-MM-dd"))) \
            .select("ind_id", "cat", "cat_sum_freq", "date")
    except AnalysisException:
        return None
    return monthly_cats


def get_cats(spark, min_date, max_date):
    partitions = list(set(['y=' + str(date.year) + '/m=' + str(date.month).zfill(2) for date in
                           [min_date + datetime.timedelta(days=delta) for delta in range((max_date - min_date).days)]]))

    individual_cats = get_monthly_cats(spark, partitions[0])
    if individual_cats is not None:
        for partition in partitions[1:]:
            monthly_cats = get_monthly_cats(spark, partition)
            if monthly_cats is not None:
                individual_cats = individual_cats \
                    .union(monthly_cats)
    else:
        schema = StructType([
            StructField("ind_id", StringType(), True),
            StructField("cat", StringType(), True),
            StructField("cat_sum_freq", IntegerType(), True),
            StructField("date", DateType(), True)
        ])
        return spark.createDataFrame(spark.sparkContext.emptyRDD(), schema)
    return individual_cats


def get_ranked(spark, from_date, to_date):
    ranked_video_event = spark.sql(f"""SELECT user_uid,
                                              business_date,
                                              video_play_request_type,
                                              os_name,
                                              video_id,
                                              tts
                                       FROM athena_da_prod.dl_da_ranked_video_event
                                       WHERE business_date BETWEEN '{from_date}' AND '{to_date}' AND
                                       user_uid IS NOT NULL""") \
        .withColumn("weekday", F.date_format("business_date", 'E'))
    return ranked_video_event


def get_device_id(spark, from_date, to_date):
    device_id = spark.sql(f"""SELECT deviceid,
                                     userid,
                                     tipodevice
                               FROM dl_mediaset.dl_enabler_device_id
                               WHERE CONCAT(y,'-',m,'-',d) BETWEEN '{from_date}' AND '{to_date}' AND
                               userid IS NOT NULL
                               GROUP BY deviceid, userid, tipodevice""")
    return device_id


def get_taxonomy(spark, taxonomy_s3_location):
    taxonomy_machine = spark \
        .read \
        .format("csv") \
        .option("header", "true") \
        .option("sep", ";") \
        .load(taxonomy_s3_location) \
        .select("id", "name", "path")

    return taxonomy_machine


def get_stacked_viewer_type(spark, df, temp_view_name, type_str):
    df.createOrReplaceTempView(temp_view_name)

    stack_string_start = "stack(4, "
    stack_string_end = f""") as (days, has_view_type_{type_str})"""
    stack_string_middle = ""

    for col in df.columns:
        if col not in ['uuid']:
            stack_string_middle = stack_string_middle + "'" + col + "', `" + col + "`, "
    stack_string_middle = stack_string_middle[:-2]

    stack_string = stack_string_start + stack_string_middle + stack_string_end
    sql_string = """select uuid, {stack_string} from {view}""".format(
        stack_string=stack_string, view=temp_view_name)
    df_stacked = spark \
        .sql(sql_string)

    return df_stacked


def create_view(_types=[], _logic="OR", view_name=None):
    where_clause = ""
    if _types:
        viewtypes = ""
        for i, t in enumerate(_types):
            viewtypes += "`core_viewertype_100`.`has_view_type_{}` = 1".format(
                t)
            if i < len(_types) - 1:
                viewtypes += " {} ".format(_logic)
        where_clause += viewtypes

        where_clause = " WHERE ({})".format(where_clause)

        view_name = "core_view_{}".format("".join(_types))

        if len(_types) > 1:
            view_name += "_{}".format(_logic.lower())

        view_schema = """
            CREATE VIEW {view} AS (
                SELECT * FROM `core_viewertype_100`{where}
            )
        """.format(where=where_clause, view=view_name)

    return view_schema


def write_df_to_aurora(logger, df, table_name, aurora_driver, schema):
    logger.info("AURORA: deleting previous table...")
    aurora_driver.mysqlcur.execute("SET foreign_key_checks = 0;")
    aurora_driver.mysqlcur.execute(
        "DROP TABLE IF EXISTS {table_name}".format(table_name=table_name))
    aurora_driver.mysqlcur.execute("SET foreign_key_checks = 1;")
    logger.info("AURORA: creating new schema...")
    aurora_driver.mysqlcur.execute(schema)
    logger.info("AURORA: writing {table_name} table...".format(
        table_name=table_name))
    aurora_driver.save_spark_dataframe(df, table_name, "append")
