import mysql.connector as mysqlc

class EasyMySQL:
    def __init__(self, database, username, password):
        self.hostname = 'dl-prod-db-aurora.cluster-c575heupz061.eu-west-1.rds.amazonaws.com'
        self.jdbcPort = 3306
        self.__user = username
        self.__password = password
        self.properties = {
            "driver": "com.mysql.jdbc.Driver"
        }
        self.__mysqlcon = mysqlc.connect(host=self.hostname,
                                         database=database,
                                         user=self.__user,
                                         password=self.__password)
        self.mysqlcur = self.__mysqlcon.cursor()
        self.jdbcUrl = "jdbc:mysql://{0}:{1}/{2}?user={3}&password={4}".format(self.hostname,
                                                                               self.jdbcPort,
                                                                               database,
                                                                               self.__user,
                                                                               self.__password)
    def get_spark_dataframe(self, spark, tablename):
        try:
            result = spark.read.format("jdbc") \
                            .option("dbtable", tablename) \
                            .option("driver", 'com.mysql.jdbc.Driver') \
                            .load(url=self.jdbcUrl)
            return result
        except AnalysisException as e:
            raise FileNotFoundError(e)

    def save_spark_dataframe(self, df, tablename, mode):
        df \
            .write \
            .jdbc(self.jdbcUrl, tablename, mode, self.properties)

    def close(self):
        self.__mysqlcon.close()