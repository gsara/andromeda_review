import json

import boto3

# ---|| THE ONLY PARAMETERS THAT SHOULD BE CONFIGURED ||--- #
CONF_BUCKET_NAME = 'mediaset-mktg'
CONF_KEY_NAME = 'emr-etl-scripts/lambda-emr-launchers/projects/dev_test/dev_test_lambda_emr_config.json'
EMR_CLUSTER_NAME = 'EMR - MKTG RTI - TEST JOB'

# ---|| BU DEPENDENT CONFIG ||--- #
TAG_BU = 'mktg'
INSTANCE_PROFILE_ROLE = 'EMR_MKTG_RTI'

# ---|| DEFAULT VALUES: CHANGE ONLY IF NECESSARY||--- #
DEFAULT_INSTANCE_TYPE = 'r4.xlarge'
DEFAULT_S3_MAX_RETRIES = '20'
MAX_NUM_CORE_NODES = 20
DEFAULT_MAXIMIZE_RESOURCE_ALLOCATION = 'true'
DEFAULT_DYNAMIC_ALLOCATION = 'true'


def get_s3_client():
    conn = boto3.client(
        service_name='s3',
        region_name='eu-west-1')
    return conn


def get_job_launcher_config():
    conn = get_s3_client()
    file_content = conn.get_object(Bucket=CONF_BUCKET_NAME, Key=CONF_KEY_NAME)['Body'].read().decode()
    job_launcher_config = json.loads(file_content)
    return job_launcher_config


def create_packages_bootstrap(packages_list):
    if len(packages_list) == 0:
        return ''
    bootstrap_cmd = '''\
#!/bin/bash

set -x -e
sudo python3 -m pip install'''
    for package in packages_list:
        bootstrap_cmd = f'{bootstrap_cmd} {package}'

    conn = get_s3_client()
    folder_path = '/'.join(CONF_KEY_NAME.split('/')[:-1])
    key_path = f'{folder_path}/bootstrap/packages_bootstrap.sh'
    conn.put_object(Bucket=CONF_BUCKET_NAME, Key=key_path, Body=str.encode(bootstrap_cmd))
    return f's3://{CONF_BUCKET_NAME}/{key_path}'


def build_bootstrap_action(packages, other):
    len_packages = len(packages['ScriptBootstrapAction']['Path'])
    len_other = len(other['ScriptBootstrapAction']['Path'])
    if len_packages == 0:
        if len_other == 0:
            return []
        else:
            return [other]
    else:
        if len_other == 0:
            return [packages]
        else:
            return [packages, other]


def check_empty_instance_type(instance_type):
    if instance_type == "":
        return DEFAULT_INSTANCE_TYPE
    return instance_type


def check_empty_s3_max_retries(max_retries):
    if max_retries == "":
        return DEFAULT_S3_MAX_RETRIES
    return max_retries


def get_safe_n_core_nodes(num_nodes):
    n_core_nodes = int(num_nodes)
    if n_core_nodes > MAX_NUM_CORE_NODES:
        return MAX_NUM_CORE_NODES
    return n_core_nodes


def get_robust_maximize_resource_allocation(max_res_all, dr_mem, ex_mem, num_ex, ex_cor):
    if dr_mem == "" or ex_mem == "" or num_ex == "" or ex_cor == "":
        return DEFAULT_MAXIMIZE_RESOURCE_ALLOCATION
    return max_res_all


def get_robust_dynamic_allocation(dyn_all, dr_mem, ex_mem, num_ex, ex_cor):
    if dr_mem == "" or ex_mem == "" or num_ex == "" or ex_cor == "":
        return DEFAULT_DYNAMIC_ALLOCATION
    return dyn_all


def lambda_handler(event, context):
    job_launcher_config = get_job_launcher_config()

    emr_cluster_name = EMR_CLUSTER_NAME
    emr_release = job_launcher_config["emr_release"]

    master_node_instance_type = check_empty_instance_type(job_launcher_config["master_node_instance_type"])
    core_nodes_instance_type = check_empty_instance_type(job_launcher_config["core_nodes_instance_type"])
    n_core_nodes = get_safe_n_core_nodes(job_launcher_config["n_core_nodes"])

    driver_memory = job_launcher_config["driver_memory"]
    executor_memory = job_launcher_config["executor_memory"]
    num_executors = job_launcher_config["num_executors"]
    executor_cores = job_launcher_config["executor_cores"]
    maximize_resource_allocation = get_robust_maximize_resource_allocation(
        job_launcher_config["maximize_resource_allocation"], driver_memory, executor_memory, num_executors,
        executor_cores)
    dynamic_allocation = get_robust_dynamic_allocation(job_launcher_config["dynamic_allocation"], driver_memory,
                                                       executor_memory, num_executors, executor_cores)

    s3_max_retries = check_empty_s3_max_retries(job_launcher_config["s3_max_retries"])

    log_path = job_launcher_config["log_path"]

    install_packages = job_launcher_config["install_packages"]
    packages_bootstrap_path = create_packages_bootstrap(install_packages)
    packages_bootstrap_action = {
        "Name": "packages_bootstrap_action",
        "ScriptBootstrapAction": {
            "Path": packages_bootstrap_path
        }
    }
    other_bootstrap_path = job_launcher_config["other_bootstrap_path"]
    other_bootstrap_action = {
        "Name": "other_bootstrap_action",
        "ScriptBootstrapAction": {
            "Path": other_bootstrap_path
        }
    }

    bootstrap_action = build_bootstrap_action(packages_bootstrap_action, other_bootstrap_action)

    spark_script_path = job_launcher_config["spark_script_path"]
    py_files_path = job_launcher_config["py_files_path"]
    files_path = job_launcher_config["files_path"]

    conn = boto3.client(
        service_name='emr',
        region_name='eu-west-1',
    )
    cluster_id = conn.run_job_flow(
        Name=emr_cluster_name,
        Applications=[{
            'Name': 'Spark'
        }],
        ServiceRole='EMR_DefaultRole',
        JobFlowRole=INSTANCE_PROFILE_ROLE,
        AutoScalingRole='EMR_AutoScaling_DefaultRole',
        VisibleToAllUsers=True,
        LogUri=log_path,
        ReleaseLabel=emr_release,
        Instances={
            'InstanceGroups': [
                {
                    'Name': 'Master node',
                    'Market': 'SPOT',
                    'InstanceRole': 'MASTER',
                    'InstanceType': master_node_instance_type,
                    'InstanceCount': 1,
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'st1',
                                    'SizeInGB': 500
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                        'EbsOptimized': True
                    }
                },
                {
                    'Name': 'Core nodes',
                    'Market': 'SPOT',
                    'InstanceRole': 'CORE',
                    'InstanceType': core_nodes_instance_type,
                    'InstanceCount': n_core_nodes,
                    'EbsConfiguration': {
                        'EbsBlockDeviceConfigs': [
                            {
                                'VolumeSpecification': {
                                    'VolumeType': 'st1',
                                    'SizeInGB': 500
                                },
                                'VolumesPerInstance': 1
                            },
                        ],
                        'EbsOptimized': True
                    }
                }
            ],
            'KeepJobFlowAliveWhenNoSteps': False,
            'TerminationProtected': False,
            'Ec2SubnetId': 'subnet-30d36b68',
            'ServiceAccessSecurityGroup': 'sg-a8cccecf',
            'EmrManagedSlaveSecurityGroup': 'sg-a9cccece',
            'EmrManagedMasterSecurityGroup': 'sg-abcccecc'
        },
        BootstrapActions=bootstrap_action,
        Configurations=[
            {
                'Classification': 'spark-hive-site',
                'Properties':
                    {
                        "hive.metastore.client.factory.class": "com.amazonaws.glue.catalog.metastore.AWSGlueDataCatalogHiveClientFactory"
                    },
                "Configurations": []
            },
            {
                'Classification': 'spark',
                'Properties': {
                    "maximizeResourceAllocation": maximize_resource_allocation
                },
            },
            {
                'Classification': 'spark-defaults',
                'Properties': {
                    "spark.dynamicAllocation.enabled": dynamic_allocation,
                    "spark.driver.memory": driver_memory,
                    "spark.executor.memory": executor_memory,
                    "spark.executor.instances": num_executors,
                    "spark.executor.cores": executor_cores
                },
            },
            {
                'Classification': 'emrfs-site',
                'Properties': {
                    "fs.s3.maxRetries": s3_max_retries
                }
            }
        ],
        Steps=[{
            'Name': 'Spark Step',
            'ActionOnFailure': 'TERMINATE_CLUSTER',
            'HadoopJarStep': {
                'Jar': 'command-runner.jar',
                'Args': [
                    'spark-submit',
                    '--py-files',
                    *py_files_path,
                    '--files',
                    *files_path,
                    '--deploy-mode',
                    'cluster',
                    '--master',
                    'yarn',
                    spark_script_path
                ]
            }
        }],
        ScaleDownBehavior='TERMINATE_AT_TASK_COMPLETION',
        Tags=[
            {
                'Key': 'Name',
                'Value': emr_cluster_name
            },
            {
                'Key': 'BU',
                'Value': TAG_BU
            }
        ]
    )
    return "Started cluster {}".format(cluster_id)
